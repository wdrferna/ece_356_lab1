import random
import math
from collections import deque

# Fixed Variables
L = 1500
C = 300000000
D = 10
R = 1000000
S = (2/3)*C
T_delay = L/R # Transmission delay for a packet
T_prop = D/S # propagation delay for a packet
nodesList = [20, 40, 60, 80, 100] # number of nodes in the LAn being tested on
 
# Generates exponential random variable based on a lambda value
def generateExpRandVar(lam): 
    uniformVar = random.random() # creates random numbers between 0 and 1
    x = -( 1 / lam ) * math.log(1-uniformVar)
    return x

# Generates exponential backoff time based on collision counter value
def generateExpBackoff(i):
	uniformVar = random.randint(0, pow(2,i)-1)
	T = 512/R # time for 512 bits
	return (T*uniformVar)	

class Node:
	def __init__(self, events):
		self.events = deque(events)
		self.collisions = 0
		self.busyBus = 0
		self.sendTime = events[0].arrivalTime # send time of the earliest packet

class Packet:
	def __init__(self, arrivalTime):
		self.arrivalTime = arrivalTime

def persistent(T):
	print('<------------------------- PERSISTENT ------------------------->')

	# efficieny and throughput variables for differnt arrival rates
	efficiency = {
		"7": [],
		"10": [],
		"20": []
	}
	throughput = {
		"7": [],
		"10": [],
		"20": []
	}
	for lmda in [7, 10, 20]:
		print()
		print('*--------------------------- Arrival Rate = ',lmda , '---------------------------*')
		for index,node in enumerate([20, 40, 60, 80, 100]):
			allNodeEvents = [] # events for all the nodes in the system
			transTotal = 0
			transTotalSuccess = 0
			simulationTimer = 0
			for i in range(0, node): # populate the nodes in the bus with random arrival rate.
				time = 0 # variable to keep track of time
				nodeEventList = []
				while (time < T): # for each node generate random arrival times
					generateArrivalTime = generateExpRandVar(lmda)
					time += generateArrivalTime
					if (time < T): # ensures that the time does not go over 1000 seconds (simulation time)
						nodeEventList.append(Packet(time))
					else:
						break				
				allNodeEvents.append(Node(nodeEventList))

			collisionCount = 0 # keeps count of collisions 	
			collision = False			
			while simulationTimer < T: # start of the simulation for simulation time
				earlistSendingTime = T
				sendNodeIndex = None
				for i in range(0, node):
					if (len(allNodeEvents[i].events) > 0): # check if the current node is not empty
						if (allNodeEvents[i].sendTime < earlistSendingTime): # check if the earliest sending time of the current node is the smallest
							earlistSendingTime = allNodeEvents[i].sendTime
							sendNodeIndex = i # if the sending time is the smallest so far, record the index of the sending node

				if (sendNodeIndex == None): # Ensures that the nodeEventList is not empty
					break
				collision = False # reset collision flag
				collisionCount = 0 # reset collision counter
				simulationTimer = allNodeEvents[sendNodeIndex].sendTime # set the current simulation time to the time of the earlist packet
 
				for i in range(0, node): # iterate through different node numbers
					if len(allNodeEvents[i].events) == 0 or i == sendNodeIndex: # check if the current node is the sender of has any events, if it is, ignore.
						continue
					else: 
						startBitTime = simulationTimer + abs(i - sendNodeIndex) * T_prop # start time of packet 
						endBitTime = simulationTimer + abs(i - sendNodeIndex)* T_prop + T_delay # end time of packet (adds transmission delay)
					# Check for collisions with other nodes
					if allNodeEvents[i].sendTime < startBitTime: # current node transmits before the first bit is received
						collisionCount += 1
						collision = True
						allNodeEvents[i].collisions += 1
						if (allNodeEvents[i].collisions > 10): # check if the collisions for a packet are > 10
							allNodeEvents[i].collision = 0 # reset collsion counter
							allNodeEvents[i].events.popleft() # remove packet
							# updating the sending time for the current node's packet
							if 	len(allNodeEvents[i].events) > 0 and allNodeEvents[i].events[0].arrivalTime > allNodeEvents[i].sendTime:
								allNodeEvents[i].sendTime = allNodeEvents[i].events[0].arrivalTime
						else:
							waitTime = generateExpBackoff(allNodeEvents[i].collisions)
							allNodeEvents[i].sendTime = max(endBitTime + waitTime, allNodeEvents[i].events[0].arrivalTime)
						transTotal += 1 # transmission counter incremented

				# Handling collisions for sending node
				if collision == True:
					collisionCount += 1 # increase collision counter
					allNodeEvents[sendNodeIndex].collisions += 1
					collision = False # reset collision flag
					# Drop packet if collisions for sending node exceeded 10
					if (allNodeEvents[sendNodeIndex].collisions > 10):
						allNodeEvents[sendNodeIndex].collision = 0
						allNodeEvents[sendNodeIndex].events.popleft()
						if 	len(allNodeEvents[sendNodeIndex].events) > 0 and allNodeEvents[sendNodeIndex].events[0].arrivalTime > allNodeEvents[sendNodeIndex].sendTime:
							allNodeEvents[sendNodeIndex].sendTime = allNodeEvents[sendNodeIndex].events[0].arrivalTime
					else:
						waitTime = generateExpBackoff(allNodeEvents[sendNodeIndex].collisions)
						allNodeEvents[sendNodeIndex].sendTime = max(simulationTimer + T_delay + waitTime, allNodeEvents[sendNodeIndex].events[0].arrivalTime)
				else:
					for i in range(0, node):
						startBitTime = simulationTimer + abs(i-sendNodeIndex)*T_prop
						endBitTime = simulationTimer + abs(i-sendNodeIndex)*T_prop + T_delay
						if len(allNodeEvents[i].events) > 0: # Checking if collision happens within send time of current node
							if (startBitTime <= allNodeEvents[i].sendTime <= endBitTime):
								allNodeEvents[i].sendTime = max(endBitTime, allNodeEvents[i].events[0].arrivalTime)

					allNodeEvents[sendNodeIndex].collisions = 0 # reset collision counter
					allNodeEvents[sendNodeIndex].events.popleft() # remove the sent node
					if len(allNodeEvents[sendNodeIndex].events) > 0 and allNodeEvents[sendNodeIndex].events[0].arrivalTime > allNodeEvents[sendNodeIndex].sendTime:
						allNodeEvents[sendNodeIndex].sendTime = allNodeEvents[sendNodeIndex].events[0].arrivalTime # updating the send time of the most recent packet
					transTotalSuccess += 1
				transTotal += 1

			remainingPackets = 0
			for node in allNodeEvents:
				remainingPackets += len(node.events)
			transTotal += remainingPackets

			effValue = transTotalSuccess/transTotal*1.0
			tPutValue = (transTotalSuccess*L)/(T*pow(10,6))			
			efficiency[str(lmda)].append(effValue)
			throughput[str(lmda)].append(tPutValue)
			print('N = ', nodesList[index], ', ', 'Efficiency = ', 1.0*transTotalSuccess/transTotal, ', ', 'Throughput = ', (transTotalSuccess*L)/(T*pow(10, 6)))
	print(efficiency)
	print(throughput)

#Non-Persistent CSMA/CD
def nonPersistent(lam, T):
	N = [20, 40, 60, 80, 100]

	# Looping through each N scenario
	for (index, current_number_of_nodes) in enumerate(N):
		# Collection of events for N nodes
		N_events = []

		# To track time spent in simulation
		simulationTimer = 0

		Total_transmissions = 0
		Total_success_transmissions = 0

		# Generating events for each node
		for i in range(0, current_number_of_nodes):

			# Counter to check if arrivaltime is greater than the simulation time
			timer = 0
			generatedEvents = []
			while (timer < T):
				generateArrivalTime = generateExpRandVar(lam)
				timer += generateArrivalTime
				if (timer < T):
					generatedEvents.append(Packet(timer))
				else:
					break
			N_events.append(Node(generatedEvents))

		collision = False

		while simulationTimer < T:
			collision = False
			lowestSendingTime = T
			sendNodeIndex = -1

			# Finding the earliest arrival time among all nodes
			for i in range(0, current_number_of_nodes):
				if (len(N_events[i].events) > 0):
					if (N_events[i].sendTime < lowestSendingTime):
						lowestSendingTime = N_events[i].sendTime
						sendNodeIndex = i
			
			# Break if there are no events to send 
			if (sendNodeIndex == -1):
				break

			simulationTimer = N_events[sendNodeIndex].sendTime
			N_events[sendNodeIndex].busyBus = 0

			#Collision detection for all nodes (except for the sending node)
			for i in range(0, current_number_of_nodes):
				if ((len(N_events[i].events) == 0) or i == sendNodeIndex):
					continue
				else:
					startBit = simulationTimer + abs(i-sendNodeIndex)*T_prop
					endBit = simulationTimer + abs(i-sendNodeIndex)*T_prop + T_delay

				if N_events[i].sendTime < startBit:
					collision = True
					N_events[i].collisions += 1
					N_events[i].busyBus = 0

					# Discarding packet if number of collisions exceeds limit
					if (N_events[i].collisions > 10):
						N_events[i].collision = 0
						N_events[i].events.popleft()

						# Updating the earliest sendtime for the node after the collision packet has been dropped
						if len(N_events[i].events) > 0 and N_events[i].events[0].arrivalTime > N_events[i].sendTime:
							N_events[i].sendTime = N_events[i].events[0].arrivalTime
					else:
						# Applying exponetional backoff
						T_wait = generateExpBackoff(N_events[i].collisions)
						N_events[i].busyBus += 1
						T_backoff_delay = generateExpBackoff(N_events[i].busyBus)
						
						# Discarding packet if number of busy bus count exceeds limit
						if (N_events[i].busyBus > 10):
							N_events[i].busyBus = 0
							N_events[i].events.popleft()
							Total_transmissions += 1
						else:
							N_events[i].sendTime = max(endBit + T_wait + T_backoff_delay, N_events[i].events[0].arrivalTime)
					
					Total_transmissions += 1
			
			# Collision detection at the sender node
			if collision == True:
				N_events[sendNodeIndex].collisions += 1
				N_events[sendNodeIndex].busyBus = 0

				# Discarding packet if number of collisions exceeds limit
				if (N_events[sendNodeIndex].collisions > 10):
					N_events[sendNodeIndex].collision = 0
					N_events[sendNodeIndex].events.popleft()

					# Updating the earliest sendtime for the node after the collision packet has been dropped
					if len(N_events[sendNodeIndex].events) > 0 and N_events[sendNodeIndex].events[0].arrivalTime > N_events[sendNodeIndex].sendTime:
						N_events[sendNodeIndex].sendTime = N_events[sendNodeIndex].events[0].arrivalTime
				else:
					# Applying exponetional backoff
					T_wait = generateExpBackoff(N_events[sendNodeIndex].collisions)
					N_events[i].busyBus += 1
					T_backoff_delay = generateExpBackoff(N_events[sendNodeIndex].busyBus)

					# Discarding packet if number of busy bus count exceeds limit
					if (N_events[i].busyBus > 10):
						N_events[i].busyBus = 0
						N_events[i].events.popleft()
						Total_transmissions += 1
					else:
						N_events[sendNodeIndex].sendTime = max(simulationTimer + T_delay + T_wait + T_backoff_delay, N_events[sendNodeIndex].events[0].arrivalTime)
			else:
				for i in range(0, current_number_of_nodes):
					startBit = simulationTimer + abs(i-sendNodeIndex)*T_prop
					endBit = simulationTimer + abs(i-sendNodeIndex)*T_prop + T_delay
					
					# Updating sendtime of current node if it lies within the transmission time of the sending node 
					if len(N_events[i].events) > 0:
						if (startBit <= N_events[i].sendTime <= endBit):
							N_events[i].busyBus += 1
							T_backoff_delay = generateExpBackoff(N_events[i].busyBus)
							# Discarding packet if number of busy bus count exceeds limit
							if (N_events[i].busyBus > 10):
								N_events[i].busyBus = 0
								N_events[i].events.popleft()
								Total_transmissions += 1
							else:
								N_events[i].sendTime = max(endBit + T_backoff_delay, N_events[i].events[0].arrivalTime)

				# Updating sendtime of sender node after successful transmission
				N_events[sendNodeIndex].collisions = 0
				N_events[sendNodeIndex].events.popleft()
				if len(N_events[sendNodeIndex].events) > 0 and N_events[sendNodeIndex].events[0].arrivalTime > N_events[sendNodeIndex].sendTime:
					N_events[sendNodeIndex].sendTime = N_events[sendNodeIndex].events[0].arrivalTime
				Total_success_transmissions += 1
			Total_transmissions += 1

		remainingPackets = 0
		for node in N_events:
			remainingPackets += len(node.events)
		Total_transmissions += remainingPackets

		efficiency = 1.0*Total_success_transmissions/Total_transmissions
		throughput = (Total_success_transmissions*L)/(T*10**6)

		print('[N = ', N[index],']', 'Efficiency = ', efficiency, 'Throughput = ', throughput)

def main():
	# Running simulation for persistent and non-persistent networks with simulation time of 1000 seconds
	simulationTime = 1000
	persistent(simulationTime)
	A = [7, 10, 20]
	print()
	print('**************************** NON_PERSISTENT (T = 1000) ****************************')
	for lam in A:
		print()
		print('<---------------------------[ A = ',lam , ']--------------------------->')
		nonPersistent(lam, simulationTime)
	# Uncomment code below to run simulation for 2000 seconds
""" 	simulationTime = 2000
	persistent(simulationTime)
	A = [7, 10, 20]
	print()
	print('**************************** NON_PERSISTENT (T = 2000) ****************************')
	for lam in A:
		print()
		print('<---------------------------[ A = ',lam , ']--------------------------->')
		nonPersistent(lam, simulationTime) """

if __name__ == '__main__':
	main()
