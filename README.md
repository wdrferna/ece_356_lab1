# **ECE 358 - Lab 2 (Persistent and non-persistent network simulator)**

The code is structured with a function for the the persistent Network and a function for the Non-persistent network. Seperate functions have been implemented to address the requirements of each question specified in the lab report. The output for each question is printed on to the console.

**In order to run the simulator code execute the following command:**

```
python3 main.py
```

or

```
source makefile
```

**Note**: when running the make file, ensure main.py is in the same directory as the make file.

**Performance metric graphs**

The required graphs for the performance metrics were been plotted in excel using the data that was outputted on the console during execution. The name of the excel file is **lab2_graphs.xlsx**.
